# 0. concept

## 0.1 [SYNTAX check] [SEMANTIC check Trigger] gitlab pipeline 

- .gitlab-ci.yml
    - SYNTAX check! (e.g. didnt miss import any lib)
    - trigger a docker environment and see whether the submitted code can be run without SYNTAX issue
    - act as a TRIGGER to AWS SAGEMAKER environment to do SEMANTIC work

- CI/CD job success -> doesnt mean it is SEMANTIC OK

```
Running with gitlab-runner 15.4.0~beta.5.gdefc7017 (defc7017)
  on green-3.shared.runners-manager.gitlab.com/default Jhc_Jxvh
Preparing the "docker+machine" executor
Using Docker executor with image python:3.8 ...
Pulling docker image python:3.8 ...
Using docker image sha256:9fa000b720b74ec45ec3762fa028e61fc462e04d4b3b2864aeded65137acedff for python:3.8 with digest python@sha256:4b0ff29da02ca06fece65691ed8981ecdb5c537ce6a864e60137101e308a99d9 ...
Preparing environment
00:03
Running on runner-jhcjxvh-project-39639403-concurrent-0 via runner-jhcjxvh-shared-1663900619-14bf55d7...
Getting source from Git repository
00:03
$ eval "$CI_PRE_CLONE_SCRIPT"
    .
    .
Checking out 3045d538 as refs/merge-requests/6/head...
Skipping Git submodules setup
Executing "step_script" stage of the job script
Using docker image sha256:9fa000b720b74ec45ec3762fa028e61fc462e04d4b3b2864aeded65137acedff for python:3.8 with digest python@sha256:4b0ff29da02ca06fece65691ed8981ecdb5c537ce6a864e60137101e308a99d9 ...
$ pip install --no-cache-dir --upgrade awscli pandas boto3 sagemaker requests
Collecting awscli
  Downloading awscli-1.25.80-py3-none-any.whl (3.9 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 3.9/3.9 MB 44.5 MB/s eta 0:00:00
    .
    .
$ python aws_sagemaker_train.py
[aws_sagemaker_train.py] train_s3: s3://sagemaker-ap-east-1-865184416664/aws_sagemaker_train_data/data/train/mnist.npz
Cleaning up project directory and file based variables
00:01
Job succeeded
```

## 0.2 [SEMANTIC check] aws Sagemaker pipeline (triggered by '.gitlab-ci.yml')

#### (Amazon SageMaker > Training jobs)

```
aws-sagemaker-trainingjob-23-02-39-57	Sep 23, 2022 02:40 UTC	4 minutes	Completed
```

#### (Amazon SageMaker > Training jobs > aws-sagemaker-trainingjob-23-02-39-57)

```
Status history

Status

Starting	Sep 23, 2022 02:40 UTC	Sep 23, 2022 02:41 UTC	Preparing the instances for training
Downloading	Sep 23, 2022 02:41 UTC	Sep 23, 2022 02:41 UTC	Downloading input data
Training	Sep 23, 2022 02:41 UTC	Sep 23, 2022 02:43 UTC	Training image download completed. Training in progress.
Uploading	Sep 23, 2022 02:43 UTC	Sep 23, 2022 02:43 UTC	Uploading generated training model
Completed	Sep 23, 2022 02:43 UTC	Sep 23, 2022 02:43 UTC	Training job completed
```

## 0.2.1 [SEMANTIC check] if SUCCESS, below is the folder structure being obtained

#### (Amazon S3 > Buckets > sagemaker-ap-east-1-865184416664 > aws-sagemaker-trainingjob-23-02-39-57/)

```
debug-output/	Folder

model/	Folder	

output/	Folder	

profiler-output/	Folder	

rule-output/	Folder	

source/	Folder	
```


## 0.2.2 [SEMANTIC check] if FAILURE, below is the folder structure being obtained

```
debug-output/	Folder	

profiler-output/	Folder	

source/	Folder
```
